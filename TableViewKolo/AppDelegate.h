//
//  AppDelegate.h
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17.12.2014.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

