//
//  ViewController.m
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17.12.2014.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [_item valueForKey:@"email"];
    
    _firstLabel.text = [_item valueForKey:@"address"];
    _secondLabel.text = [_item valueForKey:@"phone"];
    _textView.text = [_item valueForKey:@"about"];
    //testowa zmiana11
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
