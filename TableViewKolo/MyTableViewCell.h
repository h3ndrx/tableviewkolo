//
//  MyTableViewCell.h
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17/12/14.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
