//
//  main.m
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17.12.2014.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
