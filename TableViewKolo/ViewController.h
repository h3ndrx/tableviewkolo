//
//  ViewController.h
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17.12.2014.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong,nonatomic) NSDictionary *item;

@end

