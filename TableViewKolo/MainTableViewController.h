//
//  MainTableViewController.h
//  TableViewKolo
//
//  Created by Gabriel Zolnierczuk on 17/12/14.
//  Copyright (c) 2014 Gabriel Zolnierczuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTableViewCell.h"
#import "ViewController.h"

@interface MainTableViewController : UITableViewController

@end
